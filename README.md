# IDP Airship Development Sprayer Payload Design
## TABLE OF CONTENT

- [WEEK 1](#week-1)
- [WEEK 2](#week-2)
- [WEEK 3](#week-3)
- [WEEK 4](#week-4)
- [WEEK 5](#week-5)
- [WEEK 6](#week-6)
- [WEEK 7](#week-7)
- [WEEK 8](#week-8)
- [WEEK 9](#week-9)
- [WEEK 10](#week-10)
- [WEEK 11](#week-11)
- [WEEK 12](#week-12)
- [WEEK 13](#week-13)
- [WEEK 14](#week-14)

---

## CONTENT

### WEEK 1

Dr Salahuddin, our principal lecturer for this Aerospace Design Project, introduced all students during the first week. This project is basically a continuation of the Putraspace Hybrid Airship UAV (PHSAU) master's project, which we are developing by integrating a specific goal of application. Dr Salahuddin and Mr Amirul Fiqri convened a meeting to ensure that all students are informed about the project plan at an early stage.

In order for these programmes to run successfully as envisioned, numerous subsystems must be considered. The subsystems will be a group of students who will be separated into subgroups based on their strengths and interests. The subsystems includes:

- Flight system integration
- Design 
- Simulation
- Propulsion
- Control Systems
- Payload (Agricultural Sprayer)

The initial goal is to assess how well the control systems function on a small size airship, and then there will be a new larger airship that can transport payloads and do agricultural spraying when the control systems have been thoroughly built and tested. Finally, towards the end of this semester, two different scale airships will be built and tested.

### WEEK 2

#### Agenda

- In the second week, Dr Salah assigned us to different groups of interests and capabilities based on questionaires given for us to answer. There are two groups where the first groups is for the subsystem to work on the same and common goals to handle and achieve by the end of the semester, while the other groups consists of each one of the subsystems to communicate and share each others findings so that all the subsystems are aware of the development of the whole project. i was assigned in the sprayer payload design subsystem and in the group 6. 

##### Subsystem

- A table of gantt charts is created depending on the milestone objective to make it more structured. Each subsystem sets their target and goals for each week that they need to achieve. 
- We among our subsystem group (payload) had a short brief meeting to figure out what are the components needed in order for the sprayer system to load on the Hybrid Airship UAV. 
- Simple researches were done on the internet to check the availability of the components and what to get of the system. 


#### Method

- The gantt chart of each subsystems are presented in the link: https://docs.google.com/spreadsheets/d/1j9F4gbNZMCxQDzfhFpA2i7lVFxfhL9kVc-zxi1asfZs/edit#gid=1115838130
- A checklist of the components was made in order to make it more organised and steady.


#### Impact

- Preparations for each week can be made earlier.
- Easier for us to decide what to buy and to finalize our findings.

### WEEK 3

#### Agenda

- In the third week, me and my subsystem decided to have a site visit to one of the biggest company in the drone market in Malaysia which is Aerodryne.
- The visit was done in order to survey how the sprayer system works generally.
- A suitable spryer were acknowledge and found.


#### Method

- Visit was made to Aerodyne.


#### Findings 

- X5 drones were used for the agriculture pesticide spraying
- The drone consists of 4 nozzle sprayer
- 5% of the battery were used just to climb up to target altitude
- The small drone can go up to 8 meters in height
- Boomstick used can cover one plot in the mapping system which is 3 acres or 1.2 hectres or 12 lines.
- The tank (10-15L) used were custom made from the factory so it is really hard to get.

#### Impact

- Our group can gain connections with the industry.
- More information about the agricultural sprayer drones were learnt.
- Acknowledged information that cannot be found on the internet.


### WEEK 4

#### Agenda

- In the fourth week, our subsystem have made an excel sheets for the parameter calculations in order to make it easier and neatly arranged to get the exact value of flowrate, GPM, APM, etc.
- The sprayer designs and components such as sprayer, tank, etc. were discussed among ourselves so everyone could suggest their opinion.
- The suitable components for the sprayer system were also discussed based on the findings on internet and the survey done in Aerodyne with Fiqri.
- A component tracker excel sheets containing all the details about the product components was made to smoothen the process of tracking.


#### Method

- The excel sheet was done based on the Appendix given by Dr Salahuddin which contains all the calculations and formula.
- The discussions were held online in Google Meet and physically.

#### Findings

- 2 options for the sprayer:
    1. Ready-made full-set system costs RM 2500 
    2. Buy separately from online/nearby store and assemble for arounf RM 1000
- Meeting with Fiqri:
    1. A lower pump flowrate were suggested as the HAU will be moving slowly unlike drones.
    2. Could either bought a small 5L tank or custom it ourselves using the existing tank in the lab.

#### Impact

- More understanding on how the sprayer and the HAU actually works and performed.
- The decision on the components can be made accordingly with the parameters since the calculations can be made easier on the excel sheets.
- The component tracker: https://docs.google.com/spreadsheets/d/1f6qWolqoLCnQdBIxJAotW_M_l-wg4oBWc64O2xBw39E/edit?usp=sharing 


### WEEK 5

#### Agenda

- From the fourth week's meeting, the components discussed need to be changed as suggested by Fiqri.
- From the suggestions, new parameters were calculated and suitable components were chosen based on the calcultions.
- The designs for the sprayer system, locations of the nozzle and components were discussed among ourselves and presented to Dr Salahuddin and other subsystem.

#### Impact

- The component tracker mentioned before can be finalized and prepared for ordering.
- The original design suggested that the sprayer system will be using 8 nozzle with 4 nozzle paralleled in 2 boomsticks horizontally on the HAU. However, taking into consideration Dr Salahuddin's advice, there might be some downwash from the propellers, increase in unnecessary weight and pressure, we decided to change the design. 
- The designs were finalized to be just 4 nozzle paralleled on 1 boomstick hoorizontally on the middle bottom of the HAU.


### WEEK 6

#### Agenda

- During week 6, our subsystem had separated into smaller groups to smoothens things out. Each group will have their own small tasks that needs to be done.
- Meetings were done to discuss about the selections of the component to mount the system to the gondola and the design of the structure for the whole system. 
- Further calculations were done for the parameters.
- Contact with sellers and further surveys were conducted to make sure the finalized components are available and suitable with the parameters. 
- Among the other tasks conducted were: 
  1. Estimated Weight and Field Speed Query (Thebban)
  2. Calculations Transfer to Google Sheet (Yamunan)
  3. Mounting + Structure Design (Aleesya + Hanis)
  4. Nozzle Jet Swath Query with Seller (Irfan)
  5. Pesticide Mixing Ratio Calculation and conversion to GPA (Rafi)
  6. Total Calculation completion (Thebban + Yamunan)
  7. Presentation prepration for next week (Rafi)

#### Impact

- Mounting body of the sprayer systems were set to use electrical pvc pipe as it is lighter that normal pvc pipe.
- Connectors will be used to joint together the pipe to make the structure.
- Velcro strap will be used to hold the whole system together in place.
- Flex tubes will be attached to the boomstick using cable ties.
- The component tracker were updated with all the required things to buy.
- The weight of the whole system were estimated in the tracker as well. 


### WEEK 7

#### Agenda

- Some of the components for the mounting of the system to the gondola were bought from the nearest hardware store.
- Discussions were made with the subsystem group to finalize the parameter for the components such as sprayer and nozzle before ordering happen.
- The finalised list of the components were rechecked and confirmed and given to Fiqri.
- Order of the product will be done and hopefully to recieve by week 8.

#### Impact

- Some of the components bought for the mounting had to be cut and connect to make the structure.
- During the midsem break, the assembly of the mounting structure will be done.
- All the components are set and confirmed.

### WEEK 8

#### Agenda

- For this week, the assembly was done for the boomstick using the ovc and the fluxtube.
- However, a new design for the attachment of the payload with the gondola to the HAU was discussed.

#### Impact

- New ideas for the design were discussed and produced for the attachment issue.
- The new design must meet a certain criteria where it has:
  1. Battery access
  2. Power board access
  3. Stability in balance and weight distribution
  4. thruster arm connector
  5. attachment to the HAU

### WEEK 9

#### Agenda

- All activities were postponed due to the flood and covis cases that occured.

### WEEK 10

#### Agenda

- The pump recieved were exchanged at poladrone.
- All physical activities are still on hold due to the same flood and covid issues. 

### WEEK 11

#### Agenda

- During week 11, complete assembly of the payload system were done at the lab.
- The nozzle and the pump were tested.
- The test was done on a dummy tank to figure if any problem would happened before it was tested on real one.

#### Problem

However,
- There was a leakage at the T-connector
- The attachment for the pump to the tank had some issues

#### Solution

So,
- The connector at the joint was taped before connected to the flextube.
- wall plugs were used to screw the pump to the tank.

### WEEK 12

#### Agenda

- On week 12, the assembly was done on the actual tank which include the pump and the boomstick with the nozzles.
- Battery tests were conducted to know how much percentage were consumed for a full tank run.
- All the leaking problem were double check and solved.

#### Impact

- The sprayer system works as a whole and ready to be attached to the airship.

### WEEK 13

#### Agenda

- On week 13, discussions were made on how to attached the gondola to the sprayer tank.
- The plan was to attached the gondola with a plywood and use velcro as the attachment.

#### Impact

- L-bracket were added to the side of the tank to support the gondola with the plywood glued to the tank.

### WEEK 14

#### Agenda

- A switch control for the pump were developed.
- Using apps from a phone connected to the wifi moduler connected to the pump, the pump can now be control for on and off remotely.
